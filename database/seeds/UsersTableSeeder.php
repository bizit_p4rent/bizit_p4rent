<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('users')->insert(array(
	     array(
	       'name' => 'Hồ Ngọc Thanh Tâm',
	       'email' => 'tam.ho@student.passerellesnumeriques.org',
	       'phone' => '0868297062',
	       'password' => '123456',
	     ),
	   ));
	}
}
