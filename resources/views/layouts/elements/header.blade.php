<style>
	#header {
    width: 100%;
    height: auto;
    background-color: #DDDDDD;

}

#innerHeader {
    display: block;
    margin: 0 auto;
    width: 100%;
    height: 200px;
}

#logo {
    float: left;
    width: 20%;
     margin: 0 auto;
    height: 100px;
    display: inline-block;
}
#intro_menu_ws {
    float: left;
    width: 80%;
     margin: 0 auto;
    height: 100px;
    display: inline-block;
}
.ul_menu_top {
  float:right;
  width:70%;
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: #4CAF50;
}
.search_top{
  float:right;
  clear:both;
}
.intro_ws{
  clear:both;
  text-align:center;
  width:100%;
  font-size:66px;
}

</style>

<div id="wrapper">
    <div id="header">
        <div id="innerHeader">
            <div id="logo">
                Logo here
            </div>
            <div id="intro_menu_ws">
              <div class="menu_ws">
                <ul class="ul_menu_top">
                  <li><a class="active" href="#home">Home</a></li>
                  <li><a href="#news">News</a></li>
                  <li><a href="#contact">Contact</a></li>
                  <li><a href="#about">About</a></li>
                </ul>
              </div>
              <div class="search_top">Search: <input></div>
              <div class="intro_ws">Welcome to our website</div>
              
            </div>
        </div>
    </div>
</div>