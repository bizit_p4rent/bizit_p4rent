<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title')</title>
	<link rel="stylesheet" href="">

	<!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('bootstrap/css/bootstrap.min.css')}}"/>

	<!-- Main CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}"/> 
    
</head>
<body>
	<!--/header-->
	@include("layouts.elements.header")
	<!--/slider-->
	@include("layouts.elements.slide")
	<section>
		<div class="sidebar">
			@include("layouts.elements.sidebar")
		</div>
		<div class="content">
			@yield('content')
		</div>
	</section>
	@include("layouts.elements.footer")
	<!--/Footer-->

	<!-- Bootstrap core js -->
    <script src="{{url('bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Main js -->
    <script src="{{url('js/javascript.js')}}"></script>

</body>
</html>